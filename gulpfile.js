var wrench = require('wrench'),
	path = require('./front-end/gulp/gulp-path');


wrench.readdirSyncRecursive('./front-end/gulp/task').map(function (file) {
	require('./front-end/gulp/task/' + file)(path);
});
