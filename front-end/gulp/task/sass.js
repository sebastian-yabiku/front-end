'use strict';
var gulp = require('gulp'),
	sass = require('gulp-sass');

module.exports = function(options){
	var config = {
		task : {
			default : "sass"
		},
		taskWatch : {
			default : "sass:watch",
		},
		dev : {
			default : {
				sass : options.dev.sassFile
			}
		},
		prod : {
			default : {
				css : options.prod.css
			}
		}
	};



	gulp.task(config.task.default, sassTaskDefault);
	gulp.task(config.taskWatch.default , sassTaskWatchDefault);


	function sassTaskDefault()
	{

		gulp.src(config.dev.default.sass)
			.pipe(sass({
				includePaths: require('node-jeet-sass').includePaths,
				outputStyle : 'compressed'
			}).on('error', sass.logError))
			.pipe(gulp.dest(config.prod.default.css));

	}
	function sassTaskWatchDefault()
	{
		gulp.watch(config.dev.default.sass, [config.task.default]);
	}
};
