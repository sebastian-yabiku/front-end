'use strict';
var gulp = require('gulp'),
		imagemin = require('gulp-imagemin');

module.exports = function(options){
	var config = {
		task : {
			"imagemin" : "imagemin"
		},
		taskWatch : {
			"imagemin" : "imagemin:watch",
		},
		dev : {
			"imagemin" : {
				sass : options.dev.sassFile,
				image : options.dev.imageFile
			}
		},
		prod : {
			"imagemin" : {
				css : options.prod.css,
				image : options.prod.imagePath
			}
		}
	};



	gulp.task(config.task.imagemin, imageMin);
	gulp.task(config.taskWatch.imagemin , imageMinWatch);


	function imageMin()
	{
		console.log(config.dev.imagemin.image)
		console.log(config.prod.imagemin.image)
	  return gulp.src('./front-end/image/minisite/descabellado/content/*.jpg')
				  .pipe(imagemin())
				  .pipe(gulp.dest('images'));
	}
	function imageMinWatch()
	{
		//gulp.watch(config.dev.default.sass, [config.task.default]);
	}

	gulp.task('jpgs', function() {
    return gulp.src('image/*.jpg')
    .pipe(imagemin({ progressive: true }))
    .pipe(gulp.dest('image'));
});

};
