'use strict';
var gulp = require('gulp'),
	spritesmith = require('gulp.spritesmith');

module.exports = function(options){

	var config = {
		task : {
			"descabellado" : "sprite-descabellado"
		},
		taskWatch : {
			"descabellado" : "sprite-descabellado:watch"
		},
		dev : {
			"descabellado" : {
				image : options.dev.imagePath + 'minisite/descabellado/design/sprite',
				sass : options.dev.sassPath + 'minisite/descabellado/'
			}
		},
		prod : {
			"descabellado" : {
				image : options.prod.imagePath + 'minisite/descabellado/design/'
			}
		}
	};

	gulp.task(config.task.descabellado, spriteTaskDescabellado);

	function spriteTaskDescabellado()
	{
		var spriteData = gulp.src(config.dev.descabellado.image + '/*.png').pipe(spritesmith({

			imgName: 'sprite.png',
			cssName: '_sprite.scss',
			imgPath : '../../../image/minisite/descabellado/design/sprite.png',//config.prod.default.image + 'sprite.png',
			padding: 10
		}));

		console.log(config.prod.descabellado.image)
		spriteData.img.pipe(gulp.dest(config.prod.descabellado.image));
		spriteData.css.pipe(gulp.dest(config.dev.descabellado.sass));
	}
};
